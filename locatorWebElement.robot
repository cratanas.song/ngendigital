*** Settings ***
Library    Selenium2Library

*** Variable ***
${url1}    http://www.majorcineplex.com/th/main
${url2}    https://www.google.com/

*** Keywords ***
Open Web Site
    [Arguments]     ${url}
    Open Browser    ${url}      chrome
    Maximize Browser Window


Click Promotion By ID
    Click Element       xpath=//a[@id="menu_promotion"]


Input Search By Name
    Input Text          xpath=//input[@name="q"]   123
            
Input Search By Class
    Input Text          xpath=//input[@class="searchT"]     123

*** Test Case ***
TC01 
    Open Web Site    ${url1}
    Click Promotion By ID
    sleep   3s
    [Teardown]      Close Browser

TC02
    Open Web Site     ${url2}
    Input Search By Name
    sleep   3s
    [Teardown]      Close Browser

TC03 
    Open Web Site    ${url1}
    Input Search By Class
    sleep   3s
    [Teardown]      Close Browser

